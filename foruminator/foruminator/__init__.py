from foruminator.celery import app as celery_app


default_app_config = 'foruminator.apps.ForuminatorConfig'

__all__ = ['celery_app']
